//
//  KowaiUtil.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CHANGE_FAVORIT @"changeFavoritEvent"

@interface KowaiUtil : NSObject

+(void) setup:(UIViewController*)rootViewController;
+(UIViewController*)rootViewController;

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name;
+(void) dispatchGlobalEvent:(NSString*)name;

+(int) fontSizeIndex;
+(void) setFontSizeIndex:(int)index;
+(float) fontSize;

+(void) setNickname:(NSString*)nickname;
+(NSString*)nickname;

+(BOOL) isJaDevice;

+(void) tryShowRecommend;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end
