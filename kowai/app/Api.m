//
//  Api.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Api.h"

@implementation Api

+(NSString*)url:(NSString*)path{
    return [NSString stringWithFormat:@"%@/%@", API_ENDPOINT, path];
}

+(NSString*)articleJson:(int)index{
    NSString* path = [NSString stringWithFormat:@"article/%d.json", index];
    return [self url:path];
}

+(NSString*)updateArticle:(int)lastArticleId{
    NSString* path = [NSString stringWithFormat:@"articleList.php?lastArticleId=%d", lastArticleId];
    return [self url:path];
}

+(NSString*)impressionUrl{
    return [self url:@"impression.php"];
}

+(NSString*)articleImpressionUrl:(int)articleId{
    NSString* path = [NSString stringWithFormat:@"articleImpressionList.php?articleId=%d", articleId];
    return [self url:path];
}

+(NSString*)impressionListUrl{
    return [self url:@"impressionList.php"];
}

+(NSString*)ihanUrl{
    return [self url:@"ihan.php"];
}

@end
