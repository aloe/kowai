//
//  Ad.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdArticleList.h"

@interface Ad : NSObject

+(void) setup;
+(void) reload;
+(AdArticle*)adArticle;

@end
