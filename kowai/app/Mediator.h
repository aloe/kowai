//
//  Mediator.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mediator : NSObject<UIApplicationDelegate>

-(id) initWithWindow:(UIWindow*)window_;
-(void) memoryWarning;

@end
