//
//  FavoritPage.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "FavoritPage.h"
#import "ArticleListViewController.h"
#import "Model.h"
#import "ArticleViewController.h"
#import "AdViewController.h"

@interface FavoritPage ()<
ArticleListViewDelegate
, ArticleViewDelegate
, AdViewDelegate
>{
    ArticleListViewController* articleListViewController;
}

@end

@implementation FavoritPage

-(void) initialize{
    articleListViewController = [[ArticleListViewController alloc] initWithDelegate:self];
    [KowaiUtil addGlobalEventlistener:self selector:@selector(reload) name:CHANGE_FAVORIT];
}

-(UIViewController*) createRootViewController{
    return articleListViewController;
}

-(UIColor*)lineColor{
    return UIColorFromHex(0x660000);
}

-(void) reload{
    [articleListViewController reload:[[Model getInstance] favoritArticleList]];
}

#pragma mark -------- ArticleListDelegate -----------


-(void) onSelectAdArticle:(id)adArticle{
    AdViewController* adViewController = [[AdViewController alloc] initWithDelegate:self];
    [navigationController pushViewController:adViewController animated:YES];
    [VknThreadUtil mainBlock:^{
        [adViewController reload:adArticle];
    }];
}

-(void) onSelectArticle:(Article *)article{
    ArticleViewController* articleViewController = [[ArticleViewController alloc] initWithDelegate:self andArticle:article];
    [navigationController pushViewController:articleViewController animated:YES];
    [articleViewController reload];
}

@end
