//
//  Comment.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Comment.h"

@implementation Comment

@synthesize articleId;
@synthesize nickname;
@synthesize rate;
@synthesize impression;
@synthesize createdDate;
@synthesize articleTitle;

-(id) initWithDictionary:(NSDictionary *)dic{
    self = [super init];
    if(self){
        id_ = [[dic objectForKey:@"id"] intValue];
        articleId = [[dic objectForKey:@"article_id"] intValue];
        nickname = [dic objectForKey:@"nickname"];
        rate = [[dic objectForKey:@"rate"] intValue];
        impression = [dic objectForKey:@"impression"];
        articleTitle = [dic objectForKey:@"title"];
        createdDate = [NSDate dateWithTimeIntervalSince1970:[[dic objectForKey:@"created_at"] intValue]];
    }
    
    return self;
}

@end
