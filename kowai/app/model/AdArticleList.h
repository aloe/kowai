//
//  AdArticleList.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModelList.h"
#import "AdArticle.h"

@interface AdArticleList : VknModelList

-(id) initWithChkDatalist:(NSArray*)chkDataList;
-(AdArticle*)get:(int)index;
+(AdArticleList*)createDummy;

@end
