//
//  CommentList.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "CommentList.h"

@implementation CommentList

-(id) initWithArray:(NSArray *)array{
    self = [super init];
    if(self){
        for(NSDictionary* row in array){
            Comment* comment = [[Comment alloc] initWithDictionary:row];
            [self add:comment];
        }
    }
    
    return self;
}

-(Comment*)get:(int)index{
    return (Comment*)[super get:index];
}

-(Comment*)getById:(int)index{
    return (Comment*)[super get:index];
}

@end
