//
//  Article.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Article.h"

@interface Article (){
    int createdAt;
}

@end

@implementation Article

@synthesize title;
@synthesize excerpt;
@synthesize content;
@synthesize isReaded;
@synthesize dateStr;
@synthesize isFavorit;

-(id) initWithDictionary:(NSDictionary *)dic{
    self = [super init];
    if(self){
        id_ = [[dic objectForKey:@"id"] intValue];
        title = [dic objectForKey:@"title"];
        excerpt = [dic objectForKey:@"excerpt"];
        content = [dic objectForKey:@"content"];
    }
    
    return self;
}

-(id) initWithDetailResults:(FMResultSet*)results{
    self = [super init];
    if(self){
        [self setParams:results];
        content = [results stringForColumn:@"content"];
    }
    
    return self;
}

-(void) setParams:(FMResultSet*)results{
    id_ = [results intForColumn:@"id"];
    title = [results stringForColumn:@"title"];
    excerpt = [results stringForColumn:@"excerpt"];
    createdAt = [results intForColumn:@"created_at"];
    isReaded = ([results intForColumn:@"readed"] == 1);
    isFavorit = ([results intForColumn:@"favorit"] == 1);
    
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[results intForColumn:@"created_at"]];
    dateStr = [VknDateUtil dateToString:date format:@"yyyy-MM-dd"];
}

-(BOOL) isAd{
    return NO;
}

@end
