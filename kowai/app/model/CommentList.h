//
//  CommentList.h
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModelList.h"
#import "Comment.h"

@interface CommentList : VknModelList

-(id) initWithArray:(NSArray*)array;
-(Comment*)get:(int)index;
-(Comment*)getById:(int)index;

@end
