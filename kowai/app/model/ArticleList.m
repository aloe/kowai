//
//  ArticleList.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleList.h"

@implementation ArticleList

-(id) initWithArray:(NSArray *)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            Article* article = [[Article alloc] initWithDictionary:dic];
            [self add:article];
        }
    }
    
    return self;
}

-(VknModel*) createModel:(FMResultSet*)results{
    return [[Article alloc] initWithResults:results];
}

-(Article*)get:(int)index{
    return (Article*)[super get:index];
}

-(Article*)getById:(int)index{
    return (Article*)[super getById:index];
}

@end
