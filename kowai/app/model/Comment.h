//
//  Comment.h
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModel.h"

@interface Comment : VknModel

@property int articleId;
@property NSString* nickname;
@property int rate;
@property NSString* impression;
@property NSString* articleTitle;
@property NSDate* createdDate;

@end
