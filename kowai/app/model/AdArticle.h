//
//  AdArticle.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Article.h"
#import "ChkRecordData.h"

@interface AdArticle : Article

@property NSString* detailContent;
@property NSString* appId;
@property NSString* linkUrl;
@property NSString* imageUrl;

-(id) initWithChkRecordData:(ChkRecordData*)data;

@end
