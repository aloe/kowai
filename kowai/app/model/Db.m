//
//  Db.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Db.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "VknFileUtil.h"

@implementation Db

+ (Db*)getInstance {
    static Db* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Db alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        [self createTables];
        NSLog(@"addSkipBackup");
        [KowaiUtil addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:[self dbFilePath]]];
    }
    return self;
}

static NSString* initializedTableKey = @"initializedTableKey";
-(void) createTables{
    if([self isInitializedTable]) return;
    
    FMDatabase* db = [self db];
    [db open];
    
    NSString* article = @"CREATE TABLE IF NOT EXISTS article ("
    " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL "
    ", title TEXT "
    ", excerpt TEXT "
    ", content TEXT "
    ", url TEXT "
    ", created_at TEXT "
    ", readed INTEGER DEFAULT 0 "
    ", favorit INTEGER DEFAULT 0 "
    ")";
    
    [db executeUpdate:article];
    [self showError:db];
    [db close];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:initializedTableKey];
    [ud synchronize];
    
}

-(BOOL) isInitializedTable{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    return [ud boolForKey:initializedTableKey];
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(FMDatabase*) db{
    return [[FMDatabase alloc] initWithPath:[self dbFilePath]];
}

-(FMDatabaseQueue*)dbQueue{
    return [[FMDatabaseQueue alloc] initWithPath:[self dbFilePath]];
}

-(NSString*) dbFilePath{
    return [VknFileUtil documentFilePath:[self dbFileName]];
}

-(BOOL) hasError:(FMDatabase*)db{
    return ([db lastErrorCode] != 0);
}

-(void) showError:(FMDatabase*)db{
    if(![self hasError:db]) return;
    NSLog(@"dbError:%d: %@", [db lastErrorCode], [db lastErrorMessage]);
}

-(NSString*)dbFileName{
    return @"kowai.db";
}

-(void) insertArticleList:(NSArray*)articleList callback:(DbCallback_t)callback{
    
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"INSERT INTO article (id, title, excerpt, content, url, created_at) VALUES (?, ?, ?, ?, ?, ?)";
        [db beginTransaction];
        
        for(NSDictionary* row in articleList){
            [db executeUpdate:sql
             , [row objectForKey:@"id"]
             , [row objectForKey:@"title"]
             , [row objectForKey:@"excerpt"]
             , [row objectForKey:@"content"]
             , [row objectForKey:@"url"]
             , [row objectForKey:@"created_at"]
             ];
            
            [self showError:db];
        }
        
        [db commit];
        
        callback();
    }];
}

-(void) loadArticleList:(DbResultCallback_t)callback{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT id, title, excerpt, url, created_at, readed, favorit FROM article ORDER BY id DESC";
        FMResultSet* results = [db executeQuery:sql];
        [self showError:db];
        callback(results);
        [results close];
    }];
}

-(void) loadDetailArticle:(int)articleId callback:(DbResultCallback_t)callback{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM article WHERE id = %d", articleId];
        FMResultSet* results = [db executeQuery:sql];
        [results next];
        callback(results);
        [results close];
    }];
}

-(void) readArticle:(int)articleId{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"UPDATE article SET readed = 1 WHERE id = %d", articleId];
        [db executeUpdate:sql];
    }];
}

-(void) changeFavorit:(Article*)article{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        int favorit = (article.isFavorit) ? 1 : 0;
        NSString* sql = [NSString stringWithFormat:@"UPDATE article SET favorit = %d WHERE id = %d", favorit, article.id_];
        [db executeUpdate:sql];
        [self showError:db];
        
        NSLog(@"changeFavorit");
    }];
}

@end
