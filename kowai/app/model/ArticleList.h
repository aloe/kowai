//
//  ArticleList.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModelList.h"
#import "Article.h"

@interface ArticleList : VknModelList

-(Article*)get:(int)index;
-(id) initWithArray:(NSArray*)array;

@end
