//
//  Model.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Model.h"
#import "VknUrlLoadCommand.h"
#import "Api.h"
#import "Db.h"

@interface Model (){
    ModelCallback_t updateCallback;
    ModelMessageCallback_t updateProgressCallback;
    ModelFailCallback_t updateFailCallback;
    ArticleList* articleList;
    int index;
}

@end

@implementation Model

+ (Model*)getInstance {
    static Model* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Model alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(void) startLoad:(ModelCallback_t)callback progressCallback:(ModelMessageCallback_t)progressCallback failCallback:(ModelFailCallback_t)failCallback{
    updateCallback = callback;
    updateProgressCallback = progressCallback;
    updateFailCallback = failCallback;
    
    index = 1;
    [self loadRow];
}

-(void) loadRow{
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api articleJson:index]];
    c.onComplete = ^(NSData* data){
        
        NSArray* articleArray = [VknDataUtil dataToJsonArray:data];
        
        if(articleArray == nil){
            NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:isInitialLoadKey];
            [ud synchronize];
            [VknThreadUtil mainBlock:^{
                updateCallback();
            }];
            return;
        }
        
        [[Db getInstance] insertArticleList:articleArray callback:^{
            index++;
            [self loadRow];
        }];
    };
    c.onFail = ^(NSError* error){
        NSLog(@"error:%@", error.localizedDescription);
        NSLog(@"%@", error.debugDescription);
    };
    
    [c execute:nil];
}

-(void) initialLoad:(ModelCallback_t)callback failCallback:(ModelFailCallback_t)failCallback{
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api articleJson:0]];
    
    c.onComplete = ^(NSData* data){
        NSArray* articleArray = [VknDataUtil dataToJsonArray:data];
        if(articleArray == nil) return;
        
        
        [[Db getInstance] insertArticleList:articleArray callback:^{
            [VknThreadUtil mainBlock:^{
                callback();
            }];
        }];
    };
    c.onFail = ^(NSError* error){
        
    };
    
    [c execute:nil];
}

static NSString* isInitialLoadKey = @"isInitialLoadKey";
-(BOOL) isInitialLoad{
    return [[NSUserDefaults standardUserDefaults] boolForKey:isInitialLoadKey];
}

-(void) loadModel:(ModelCallback_t)callback{
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] loadArticleList:^(FMResultSet *results) {
            articleList = [[ArticleList alloc] initWithResults:results];
            [VknThreadUtil mainBlock:^{
                callback();
            }];
        }];
    }];
}

-(ArticleList*)articleList{
    return articleList;
}

-(void) updateModel:(ModelCallback_t)callback{
    int lastArticleId = [articleList get:0].id_;
    NSString* url = [Api updateArticle:lastArticleId];
    
    NSLog(@"updateModelUrl:%@", url);
    
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:url];
    c.onComplete = ^(NSData* data){
        NSDictionary* dic = [VknDataUtil dataToJson:data];
        if(dic == nil) return;
        NSArray* articleArray = [dic objectForKey:@"article"];
        if(articleArray == nil) return;
        
        [[Db getInstance] insertArticleList:articleArray callback:^{
            [self loadModel:^{
                callback();
            }];
        }];
    };
    c.onFail = ^(NSError* error){
        
    };
    
    [c execute:nil];
}

-(void) createDetailArticle:(int)articleId callback:(ModelArticleCallback_t)callback{
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] loadDetailArticle:articleId callback:^(FMResultSet *results) {
            Article* article = [[Article alloc] initWithDetailResults:results];
            [VknThreadUtil mainBlock:^{
                callback(article);
            }];
        }];
    }];
}

-(void) readArticle:(int)articleId{
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] readArticle:articleId];
    }];
}

-(void) changeFavorit:(Article*)article{
    [KowaiUtil dispatchGlobalEvent:CHANGE_FAVORIT];
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] changeFavorit:article];
    }];
}

-(ArticleList*)favoritArticleList{
    ArticleList* list = [[ArticleList alloc] init];
    for(int i=0,max=[articleList count];i<max;i++){
        Article* article = [articleList get:i];
        if(article.isFavorit){
            [list add:article];
        }
    }
    
    return list;
}

@end
