//
//  Article.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModel.h"

@interface Article : VknModel

@property NSString* title;
@property NSString* excerpt;
@property NSString* content;
@property NSString* dateStr;
@property BOOL isReaded;
@property BOOL isFavorit;

-(id) initWithDetailResults:(FMResultSet*)results;
-(BOOL) isAd;

@end
