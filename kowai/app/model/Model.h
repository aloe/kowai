//
//  Model.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticleList.h"

typedef void (^ModelCallback_t)();
typedef void (^ModelArticleCallback_t)(Article* article);
typedef void (^ModelMessageCallback_t)(NSString* msg);
typedef void (^ModelFailCallback_t)();

@interface Model : NSObject

+(Model*)getInstance;
-(void) startLoad:(ModelCallback_t)callback
 progressCallback:(ModelMessageCallback_t)progressCallback
     failCallback:(ModelFailCallback_t)failCallback;

-(void) initialLoad:(ModelCallback_t)callback failCallback:(ModelFailCallback_t)failCallback;
-(BOOL) isInitialLoad;

-(void) updateModel:(ModelCallback_t)callback;
-(void) readArticle:(int)articleId;

-(void) loadModel:(ModelCallback_t)callback;
-(ArticleList*)articleList;

-(void) createDetailArticle:(int)articleId callback:(ModelArticleCallback_t)callback;

-(void) changeFavorit:(Article*)article;
-(ArticleList*)favoritArticleList;

@end
