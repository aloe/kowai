//
//  AdArticle.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdArticle.h"

@implementation AdArticle
@synthesize title;
@synthesize content;
@synthesize appId;
@synthesize linkUrl;
@synthesize imageUrl;
@synthesize detailContent;

-(id) initWithChkRecordData:(ChkRecordData*)data{
    self = [super init];
    if(self){
        title = data.title;
        content = data.description;
        imageUrl = data.imageIcon;
        linkUrl = data.linkUrl;
        detailContent = data.detail;
        appId = data.appStoreId;
    }
    
    return self;
}

-(BOOL) isAd{
    return NO;
}

@end
