//
//  Db.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"
#import "Article.h"

typedef void (^DbCallback_t)();
typedef void (^DbResultCallback_t)(FMResultSet* results);

@interface Db : NSObject

+(Db*)getInstance;
-(void) insertArticleList:(NSArray*)articleList callback:(DbCallback_t)callback;
-(void) loadArticleList:(DbResultCallback_t)callback;
-(void) loadDetailArticle:(int)articleId callback:(DbResultCallback_t)callback;

-(void) readArticle:(int)articleId;
-(void) changeFavorit:(Article*)article;

@end
