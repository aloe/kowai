//
//  ImpressionPage.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ImpressionPage.h"
#import "ImpressionListViewController.h"
#import "Model.h"
#import "ArticleViewController.h"
#import "AdViewController.h"
#import "VknUrlLoadCommand.h"
#import "Api.h"

@interface ImpressionPage ()<
ImpressionListViewDelegate
, ArticleViewDelegate>{
    ImpressionListViewController* impressionListViewController;
}

@end

@implementation ImpressionPage

-(void) initialize{
    impressionListViewController = [[ImpressionListViewController alloc] initWithDelegate:self];
    
    [KowaiUtil addGlobalEventlistener:self selector:@selector(reload) name:CHANGE_FAVORIT];
}

-(UIViewController*) createRootViewController{
    return impressionListViewController;
}

-(void) reload{
    
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api impressionListUrl]];
    c.onComplete = ^(NSData* data){
        NSArray* array = [VknDataUtil dataToJsonArray:data];
        if(array == nil) return;
        
        CommentList* commentList = [[CommentList alloc] initWithArray:array];
        [VknThreadUtil mainBlock:^{
            [impressionListViewController reload:commentList];
        }];
    };
    c.onFail = ^(NSError* error){
        
    };
    [c execute:nil];
    
}

#pragma mark -------- ArticleListDelegate -----------

-(void) onSelectAdArticle:(id)adArticle{
    AdViewController* adViewController = [[AdViewController alloc] initWithDelegate:self];
    [navigationController pushViewController:adViewController animated:YES];
    [VknThreadUtil mainBlock:^{
        [adViewController reload:adArticle];
    }];
}

-(void) onSelectComment:(Comment *)comment{
    Article* article = [[Article alloc] init];
    article.id_ = comment.articleId;
    article.title = comment.articleTitle;
    ArticleViewController* articleViewController = [[ArticleViewController alloc] initWithDelegate:self andArticle:article];
    
    [navigationController pushViewController:articleViewController animated:YES];
    [articleViewController reload];
}

@end
