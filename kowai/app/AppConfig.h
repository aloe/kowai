//
//  AppConfig.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

// アドステア
#define ADSTIR_MEDIA_ID @"MEDIA-44e963f3"
#define ADSTIR_SPOT_ID_LIST 1
#define ADSTIR_SPOT_ID_PAGE 2
#define ADSTIR_SPOT_ID_ALWAYS 3

// api
#define API_ENDPOINT @"http://kowai.nowhappy.info/api"

// レビュー訴求で飛ぶとこ
#define APPSTORE_URL @"https://itunes.apple.com/us/app/yue-lan-zhu-yi-sa-luoninaranai/id909584200?l=ja&ls=1&mt=8"

// アナリティクス
#define GA_TRACKCODE @"UA-53863582-1"

@interface AppConfig : NSObject


@end
