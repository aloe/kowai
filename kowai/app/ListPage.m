//
//  ListPage.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ListPage.h"
#import "ArticleListViewController.h"
#import "Model.h"
#import "ArticleViewController.h"
#import "AdViewController.h"

@interface ListPage ()<
ArticleListViewDelegate
, ArticleViewDelegate>{
    ArticleListViewController* articleListViewController;
}

@end

@implementation ListPage

-(void) initialize{
    articleListViewController = [[ArticleListViewController alloc] initWithDelegate:self];
    
    [KowaiUtil addGlobalEventlistener:self selector:@selector(reload) name:CHANGE_FAVORIT];
}

-(UIViewController*) createRootViewController{
    return articleListViewController;
}

-(void) reload{
    [articleListViewController reload:[[Model getInstance] articleList]];
}

#pragma mark -------- ArticleListDelegate -----------

-(void) onSelectAdArticle:(id)adArticle{
    AdViewController* adViewController = [[AdViewController alloc] initWithDelegate:self];
    [navigationController pushViewController:adViewController animated:YES];
    [VknThreadUtil mainBlock:^{
        [adViewController reload:adArticle];
    }];
}

-(void) onSelectArticle:(Article *)article{
    ArticleViewController* articleViewController = [[ArticleViewController alloc] initWithDelegate:self andArticle:article];
    
    [navigationController pushViewController:articleViewController animated:YES];
    [articleViewController reload];
}

@end
