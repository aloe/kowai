//
//  SettingPage.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "SettingPage.h"
#import "SettingViewController.h"

@interface SettingPage ()<
SettingViewDelegate
>{
    SettingViewController* settingViewController;
}

@end

@implementation SettingPage

-(void) initialize{
    settingViewController = [[SettingViewController alloc] initWithDelegate:self];
}

-(UIColor*)lineColor{
    return UIColorFromHex(0x990000);
}

-(UIViewController*) createRootViewController{
    return settingViewController;
}

-(void) reload{

}

@end
