//
//  Mediator.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Mediator.h"
#import "RootViewController.h"
#import "Model.h"
#import "ArticleListViewController.h"
#import "ArticleViewController.h"
#import "Menu.h"
#import "ListPage.h"
#import "ImpressionPage.h"
#import "FavoritPage.h"
#import "SettingPage.h"
#import "Api.h"
#import "VknPostCommand.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#import "Ad.h"

@interface Mediator ()<
RootViewDelegate
, PageDelegate
, MenuDelegate
, UIScrollViewDelegate
>{
    UIWindow* window;
    RootViewController* rootViewController;
    Menu* menu;
    UIScrollView* mainSv;
    NSArray* pageList;
}

@end

@implementation Mediator

-(id) initWithWindow:(UIWindow*)window_{
    self = [super init];
    if(self){
        window = window_;
        [self initialize];
        [self registerGlobalEvent];
    }
    
    return self;
}

-(void) initialize{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    rootViewController = [[RootViewController alloc] initWithDelegate:self];
    window.rootViewController = rootViewController;
    
    [KowaiUtil setup:rootViewController];
    
    UIView* rootView = rootViewController.view;
    mainSv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, 320, [VknDeviceUtil windowHeight]-64)];
    mainSv.backgroundColor = [UIColor blackColor];
    mainSv.pagingEnabled = YES;
    mainSv.delegate = self;
    mainSv.showsHorizontalScrollIndicator = NO;
    [rootView addSubview:mainSv];
    
    NSMutableArray* tmpPages = [[NSMutableArray alloc] init];
    
    ListPage* listPage = [[ListPage alloc] initWithDelegate:self index:0];
    [mainSv addSubview:[listPage getView]];
    [tmpPages addObject:listPage];
    
    ImpressionPage* impressionPage = [[ImpressionPage alloc] initWithDelegate:self index:1];
    [mainSv addSubview:[impressionPage getView]];
    [tmpPages addObject:impressionPage];
    
    FavoritPage* favoritPage = [[FavoritPage alloc] initWithDelegate:self index:2];
    [mainSv addSubview:[favoritPage getView]];
    [tmpPages addObject:favoritPage];
    
    SettingPage* settingPage = [[SettingPage alloc] initWithDelegate:self index:3];
    [mainSv addSubview:[settingPage getView]];
    [tmpPages addObject:settingPage];
    
    pageList = [NSArray arrayWithArray:tmpPages];
    
    mainSv.contentSize = CGSizeMake(320*[pageList count], mainSv.frame.size.height);
    
    menu = [[Menu alloc] initWithDelegate:self];
    [rootView addSubview:[menu getView]];
    
    [Ad setup];
    
    // analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 10;
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    [[GAI sharedInstance] trackerWithTrackingId:GA_TRACKCODE];
    [self trackScreen:@"トップ"];
    
    if([[Model getInstance] isInitialLoad]){
        [[Model getInstance] loadModel:^{
            NSLog(@"model loaded-------------");
            [self reloadPages];
            [rootViewController hideLoading];
            [self updateModel];
        }];
        return;
    }
    
    [self initialLoad];
}

-(void) trackScreen:(NSString*)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}


-(void) registerGlobalEvent{
    NSNotification *n = [NSNotification notificationWithName:@"Tuchi" object:self];
    
}

-(void) initialLoad{
    [rootViewController showLoading];
    [[Model getInstance] initialLoad:^{
        NSLog(@"initialLoaded--------------");
        [self startLoad];
    } failCallback:^{
        
    }];
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"ご確認ください";
    alertView.message = @"当アプリはユーザからのコメントを受付ているため、不適切な内容が含まれることがあります。。\nあらかじめご了承ください";
    [alertView addButtonWithTitle:@"OK"];
    [alertView show];
}

-(void) startLoad{
    [[Model getInstance] startLoad:^{
        [Ad reload];
        [[Model getInstance] loadModel:^{
            [self reloadPages];
            [[Model getInstance] updateModel:^{
                [self reloadPages];
                [rootViewController hideLoading];
            }];
        }];
        
    } progressCallback:^(NSString *msg) {
        
    } failCallback:^{
        
    }];
}

-(void) loadModel{
    [[Model getInstance] loadModel:^{
        NSLog(@"model loaded-------------");
        [self reloadPages];
        [rootViewController hideLoading];
    }];
}

-(void) updateModel{
    [[Model getInstance] updateModel:^{
        NSLog(@"updatedModel --------------");
        [self reloadPages];
        [Ad reload];
        
        [KowaiUtil tryShowRecommend];
    }];
}

-(void) reloadPages{
    for(Page* page in pageList){
        [page reload];
    }
}

-(void) setupViews{
    
}

#pragma mark -------- MenuDelegate -----------

-(void) onChangeMenu:(int)index{
    [mainSv setContentOffset:CGPointMake(320*index, 0) animated:YES];
}

#pragma mark -------- UIScrollViewDelegate -----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int page = (scrollView.contentOffset.x + 160) / 320;
    if(page < 0) page = 0;
    if(page > 3) page = 3;
    
    [menu active:page];
}

#pragma mark -------- PageDelegate -----------

-(void) onShowArticle{
    mainSv.scrollEnabled = NO;
}

-(void) onHideArticle{
    mainSv.scrollEnabled = YES;
}


#pragma mark -------- UIApplicationDelegate -----------


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self updateModel];
}

-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* deviceTokenStr = [VknDataUtil deviceTokenToStr:deviceToken];
    NSLog(@"deviceTokenStr:%@", deviceTokenStr);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSDictionary* param = @{
                            @"devicetoken": deviceTokenStr
                            , @"version": version
                            };
    
    NSString* registerUrl = [Api url:@"registerToken.php"];
    NSLog(@"%@", registerUrl);
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:param withUrl:registerUrl];
    c.onComplete = ^(NSData* data){
        NSLog(@"registered");
        NSLog(@"%@", [VknDataUtil dataToString:data]);
    };
    c.onFail = ^(NSError* error){
        NSLog(@"fail:%@", error.debugDescription);
    };
    [c execute:nil];
    
}

-(void) memoryWarning{
    [VknImageCache clear];
}


@end
