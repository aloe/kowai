//
//  RatingView.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "RatingView.h"

@interface RatingView (){
    NSArray* starList;
    int currentRate;
}

@end

@implementation RatingView

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
//    39 × 37
    CGRect frame = CGRectMake(0, 0, 19.5f, 18.5f);
    for(int i=0;i<5;i++){
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        [button setImage:[UIImage imageNamed:@"starOff"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"starOn"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = frame;
        frame.origin.x += frame.size.width + 5;
        [tmp addObject:button];
        [self addSubview:button];
        
        if(i==0)
            button.selected = YES;
    }
    
    currentRate = 1;
    self.frame = CGRectMake(0, 0, frame.origin.x, 18.5f);
    starList = [NSArray arrayWithArray:tmp];
}

-(void) tapButton:(UIButton*)button{
    int index = button.tag;
    currentRate = index+1;
    for(int i=0;i<[starList count];i++){
        UIButton* button = [starList objectAtIndex:i];
        button.selected = (i<=index);
    }
}

#pragma mark -------- public -----------

-(int) rating{
    return currentRate;
}

@end
