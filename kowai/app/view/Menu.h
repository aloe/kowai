//
//  Menu.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuDelegate <NSObject>

-(void) onChangeMenu:(int)index;

@end

@interface Menu : NSObject

-(id) initWithDelegate:(NSObject<MenuDelegate>*)delegate_;
-(UIView*)getView;
-(void) active:(int)index;

@end
