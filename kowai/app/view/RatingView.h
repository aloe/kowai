//
//  RatingView.h
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingView : UIView

-(int) rating;

@end
