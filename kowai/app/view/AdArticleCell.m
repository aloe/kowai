//
//  AdArticleCell.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdArticleCell.h"

@interface AdArticleCell (){
    __weak NSObject<AdArticleCellDelegate>* delegate;
    
    AdArticle* adArticle;
    UIView* bgView;
    UIImageView* imageView;
    UILabel* titleLabel;
    UILabel* contentLabel;
    UIView* overlay;
    UIView* bgLine;
}

@end

@implementation AdArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) setDelegate:(NSObject<AdArticleCellDelegate>*)delegate_{
    delegate = delegate_;
}

-(void) initialize{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView* view = self.contentView;
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [view addSubview:bgView];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 74, 74)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 15;
    [view addSubview:imageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 13, 207, 20)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    titleLabel.textColor = [UIColor whiteColor];
    [view addSubview:titleLabel];
    
    contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 33, 207, 54)];
    contentLabel.numberOfLines = 3;
    contentLabel.font = [UIFont systemFontOfSize:14.0f];
    contentLabel.textColor = UIColorFromHex(0xcccccc);
    [view addSubview:contentLabel];
    
    overlay = [[UIView alloc] initWithFrame:bgView.frame];
    overlay.backgroundColor = UIColorFromHex(0xcccccc);
    overlay.alpha = 0;
    [view addSubview:overlay];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 99, 320, 1)];
    line.backgroundColor = UIColorFromHex(0xff0000);
    [self.contentView addSubview:line];
    
    bgLine = [[UIView alloc] initWithFrame:line.frame];
    bgLine.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgLine];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
}

-(void) tap{
    overlay.alpha = 0.5f;
    [UIView animateWithDuration:0.4f animations:^{
        overlay.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
    }];
    
    [delegate onSelectAdArticle:adArticle];
}

-(void) reload:(AdArticle*)adArticle_ index:(int)index{
    adArticle = adArticle_;
    
    float alpha = (float)(index % 15) / 15.0f;
    if(index % 30 > 14){
        alpha = 1.0f - alpha;
    }
    bgView.backgroundColor = [UIColor colorWithRed:alpha*0.5 green:0.0f blue:0.0f alpha:1.0f];
    bgLine.alpha = alpha;
    
    titleLabel.text = [NSString stringWithFormat:@"【PR】%@", adArticle.title];
    contentLabel.text = adArticle.content;
    
    imageView.image = nil;
    [VknImageCache loadImage:adArticle.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:adArticle.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
}

@end
