//
//  ArticleCell.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"
#import "Comment.h"

@protocol ArticleCellDelegate <NSObject>

@end

@interface ArticleCell : UITableViewCell

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_;
-(void) reload:(Article*)article_ index:(int)index;
-(void) reloadComment:(Comment*)comment_ index:(int)index;
-(void) light;

@end
