//
//  AdArticleCell.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdArticle.h"

@protocol AdArticleCellDelegate <NSObject>

-(void) onSelectAdArticle:(AdArticle*)adArticle;

@end

@interface AdArticleCell : UITableViewCell

-(void) reload:(AdArticle*)adArticle_ index:(int)index;
-(void) setDelegate:(NSObject<AdArticleCellDelegate>*)delegate_;

@end