//
//  Menu.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Menu.h"

@interface MenuTab : UIView{
    UILabel* label;
}

@end

@implementation MenuTab

-(id) init{
    self = [super init];
    if(self){
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColorFromHex(0x777777);
        label.font = [UIFont boldSystemFontOfSize:15.0f];
        label.numberOfLines = 2;
        [self addSubview:label];
    }
    
    return self;
}

-(void) setTitle:(NSString*)title{
    label.text = title;
}

-(void) active{
    label.textColor = [UIColor whiteColor];
}

-(void) deactive{
    label.textColor = UIColorFromHex(0x777777);
}

+(MenuTab*)createList{
    MenuTab* tab = [[MenuTab alloc] init];
    [tab setTitle:@"一覧"];
    
    return tab;
}

+(MenuTab*)createImpression{
    MenuTab* tab = [[MenuTab alloc] init];
    [tab setTitle:@"みんなの\n評価"];
    
    return tab;
}

+(MenuTab*)createFavorit{
    MenuTab* tab = [[MenuTab alloc] init];
    [tab setTitle:@"お気に入り"];
    
    return tab;
}

+(MenuTab*)createSetting{
    MenuTab* tab = [[MenuTab alloc] init];
    [tab setTitle:@"設定"];
    
    return tab;
}

@end

@interface Menu (){
    __weak NSObject<MenuDelegate>* delegate;
    UIView* view;
    UIView* statusBar;
    NSArray* tabList;
}

@end

@implementation Menu

-(id) initWithDelegate:(NSObject<MenuDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    statusBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    statusBar.backgroundColor = UIColorFromHex(0x555555);
    [view addSubview:statusBar];
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    
    CGRect tabFrame = CGRectMake(0, 20, 80, 44);
    
    float saturation = 0.5f;
    float brightness = 0.5f;
    // 一覧
    MenuTab* listTab = [MenuTab createList];
    listTab.tag = 0;
    [listTab active];
    listTab.backgroundColor = UIColorFromHex(0x000000);
    listTab.frame = tabFrame;
    [listTab addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)]];
    tabFrame.origin.x += 80;
    [view addSubview:listTab];
    [tmp addObject:listTab];
    
    // 評価
    MenuTab* impressionTab = [MenuTab createImpression];
    [impressionTab addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)]];
    impressionTab.tag = 1;
    impressionTab.backgroundColor = UIColorFromHex(0x330000);
    impressionTab.frame = tabFrame;
    tabFrame.origin.x += 80;
    [view addSubview:impressionTab];
    [tmp addObject:impressionTab];
    
    // お気に入り
    MenuTab* favoritTab = [MenuTab createFavorit];
    [favoritTab addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)]];
    favoritTab.tag = 2;
    favoritTab.backgroundColor = UIColorFromHex(0x660000);
    favoritTab.frame = tabFrame;
    tabFrame.origin.x += 80;
    [view addSubview:favoritTab];
    [tmp addObject:favoritTab];
    
    // 設定
    MenuTab* settingTab = [MenuTab createSetting];
    [settingTab addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)]];
    settingTab.tag = 3;
    settingTab.backgroundColor = UIColorFromHex(0x990000);
    settingTab.frame = tabFrame;
    tabFrame.origin.x += 80;
    [view addSubview:settingTab];
    [tmp addObject:settingTab];
    
    tabList = [NSArray arrayWithArray:tmp];
}

-(void) tapMenu:(UITapGestureRecognizer*)reco{
    int index = reco.view.tag;
    [delegate onChangeMenu:index];
    [self active:index];
}

#pragma mark -------- public -----------

-(UIView*)getView{
    return view;
}

-(void) active:(int)index{
    for(MenuTab* tab in tabList){
        [tab deactive];
        if(tab.tag == index){
            [tab active];
        }
    }
}

@end
