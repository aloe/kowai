//
//  CommentView.h
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentList.h"

@interface CommentView : UIView

-(void) reload:(CommentList*)commentList_;

@end
