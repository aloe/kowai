//
//  ArticleCell.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleCell.h"

@interface StartView : UIView{
    NSArray* starList;
}

@end

@implementation StartView

-(id)init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    CGRect frame = CGRectMake(0, 00, 19.5f, 18.5f);
    for(int i=0;i<5;i++){
        UIImageView* star = [[UIImageView alloc] init];
        star.image = [UIImage imageNamed:@"starOn"];
        star.frame = frame;
        frame.origin.x += frame.size.width + 5;
        [tmp addObject:star];
        [self addSubview:star];
    }
    starList = [NSArray arrayWithArray:tmp];
}

-(void) update:(int)rate{
    for(int i=0;i<5;i++){
        UIImageView* imageView = [starList objectAtIndex:i];
        imageView.hidden = (i >= rate);
    }
}

@end

@interface ArticleCell (){
    __weak NSObject<ArticleCellDelegate>* delegate;
    Article* article;
    Comment* comment;
    UILabel* titleLabel;
    UILabel* excerptLabel;
    UILabel* dateLabel;
    UIView* overlay;
    UIView* bgView;
    UIView* bgLine;
    StartView* starView;
    
    UIImageView* kidoku;
}

@end

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    bgView.backgroundColor = UIColorFromHex(0x990000);
    [self.contentView addSubview:bgView];
    
    UIView* wrapView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 80)];
    [self.contentView addSubview:wrapView];
    
    kidoku = [[UIImageView alloc] initWithFrame:CGRectMake(300-53.5f, 80-53.5f, 53.5f, 53.5f)];
    kidoku.image = [UIImage imageNamed:@"kidoku"];
    kidoku.hidden = YES;
    [wrapView addSubview:kidoku];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    titleLabel.textColor = [UIColor whiteColor];
    [wrapView addSubview:titleLabel];
    
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 0, 90, 15)];
    dateLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    dateLabel.textColor = UIColorFromHex(0xffffff);
    dateLabel.textAlignment = NSTextAlignmentRight;
    [wrapView addSubview:dateLabel];
    
    starView = [[StartView alloc] init];
    starView.frame = CGRectMake(0, 25, 300, 20);
    [wrapView addSubview:starView];
    
    excerptLabel = [[UILabel alloc] init];
    excerptLabel.font = [UIFont systemFontOfSize:12.0f];
    excerptLabel.textColor = UIColorFromHex(0xcccccc);
    [wrapView addSubview:excerptLabel];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 99, 320, 1)];
    line.backgroundColor = UIColorFromHex(0xff0000);
    [self.contentView addSubview:line];
    
    bgLine = [[UIView alloc] initWithFrame:line.frame];
    bgLine.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgLine];
    
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    overlay.backgroundColor = UIColorFromHex(0xcccccc);
    overlay.alpha = 0;
    [self.contentView addSubview:overlay];
}

#pragma mark -------- public -----------

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_{
    delegate = delegate_;
}

-(void) reload:(Article*)article_ index:(int)index{
    excerptLabel.numberOfLines = 3;
    excerptLabel.frame = CGRectMake(0, 20, 300, 60);
    article = article_;
    titleLabel.text = article.title;
    excerptLabel.text = article.excerpt;
    dateLabel.text = article.dateStr;
    kidoku.hidden = !article.isReaded;
    starView.hidden = YES;
    kidoku.alpha = 1.0f;
    
    float alpha = (float)(index % 15) / 15.0f;
    if(index % 30 > 14){
        alpha = 1.0f - alpha;
    }
    
    bgView.backgroundColor = [UIColor colorWithRed:alpha*0.5 green:0.0f blue:0.0f alpha:1.0f];
    bgLine.alpha = alpha;
}

-(void) reloadComment:(Comment*)comment_ index:(int)index{
    comment = comment_;
    excerptLabel.numberOfLines = 2;
    kidoku.alpha = 0;
    excerptLabel.frame = CGRectMake(0, 40, 300, 50);
    titleLabel.text = comment.articleTitle;
    excerptLabel.text = comment.impression;
    dateLabel.text = comment.nickname;
    kidoku.hidden = YES;
    starView.hidden = NO;
    [starView update:comment.rate];
    
    float alpha = (float)(index % 15) / 15.0f;
    if(index % 30 > 14){
        alpha = 1.0f - alpha;
    }
    
    bgView.backgroundColor = [UIColor colorWithRed:alpha*0.5 green:0.0f blue:0.0f alpha:1.0f];
    bgLine.alpha = alpha;
}

-(void) light{
    article.isReaded = YES;
    kidoku.hidden = NO;
    overlay.alpha = 0.8f;
    [UIView animateWithDuration:0.4f animations:^{
        overlay.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

@end
