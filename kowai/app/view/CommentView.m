//
//  CommentView.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "CommentView.h"
#import "VknPostCommand.h"
#import "Api.h"

@interface CV : UIView{
    Comment* comment;
}

-(id) initWithComment:(Comment*)comment_;

@end

@implementation CV

-(id) initWithComment:(Comment *)comment_{
    self = [super init];
    if(self){
        comment = comment_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UILabel* nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 15)];
    nicknameLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    nicknameLabel.text = comment.nickname;
    [self addSubview:nicknameLabel];
    [self addSubview:[self createStar]];
    
    UILabel* dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 160, 18.5f)];
    dateLabel.font = [UIFont systemFontOfSize:12.0f];
    dateLabel.text = [VknDateUtil dateToString:comment.createdDate format:@"yyyy-MM-dd HH:mm"];
    [self addSubview:dateLabel];
    
    UIButton* ihanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [ihanButton setImage:[UIImage imageNamed:@"ihan"] forState:UIControlStateNormal];
    ihanButton.frame = CGRectMake(300-60, 0, 65, 50);
    [ihanButton addTarget:self action:@selector(tapIhan:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:ihanButton];
    
    UILabel* impressionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 300, 10000)];
    impressionLabel.numberOfLines = 0;
    impressionLabel.font = [UIFont systemFontOfSize:12.0f];
    impressionLabel.text = comment.impression;
    [impressionLabel sizeToFit];
    [self addSubview:impressionLabel];
    
    float height = 70 + impressionLabel.frame.size.height + 10;
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(10, height-1, 300, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self addSubview:line];
    
    self.frame = CGRectMake(0, 0, 320, height);
}

-(void) tapIhan:(UIButton*)button{
    NSLog(@"違反");
    
    button.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    [UIView animateWithDuration:0.2f animations:^{
        button.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    } completion:^(BOOL finished) {
        
    }];
    
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:@{
                                                                 @"rating_id":[NSNumber numberWithInt:comment.id_]
                                                                 , @"impression":comment.impression
                                                                 }
                                                       withUrl:[Api ihanUrl]];
    
    NSLog(@"url:%@", [Api ihanUrl]);
    c.onComplete = ^(NSData* data){
        NSLog(@"%@", [VknDataUtil dataToString:data]);
    };
    c.onFail = ^(NSError* error){
        NSLog(@"fail:%@", error.localizedDescription);
    };
    [c execute:nil];
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"ご協力ありがとうございます";
    alertView.message = @"違反報告をしました";
    [alertView addButtonWithTitle:@"OK"];
    [alertView show];
    
    
}

-(UIView*)createStar{
    UIView* starView = [[UIView alloc] init];
    CGRect frame = CGRectMake(10, 30, 19.5f, 18.5f);
    for(int i=0;i<comment.rate;i++){
        UIImageView* star = [[UIImageView alloc] init];
        star.image = [UIImage imageNamed:@"starOn"];
        star.frame = frame;
        frame.origin.x += frame.size.width + 5;
        [self addSubview:star];
    }
    
    return starView;
}

@end

@interface CommentView (){
    NSArray* commentViewList;
    CommentList* commentList;
}

@end

@implementation CommentView

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    
}

-(void) reload:(CommentList*)commentList_{
    commentList = commentList_;
    
    NSArray* subviews = self.subviews;
    for(UIView* view in subviews){
        [view removeFromSuperview];
    }
    
    float y = 0;
    for(int i=0,max=[commentList count];i<max;i++){
        Comment* comment = [commentList get:i];
        CV* cv = [[CV alloc] initWithComment:comment];
        cv.transform = CGAffineTransformMakeTranslation(0, y);
        [self addSubview:cv];
         y += cv.frame.size.height;
    }
    
    self.frame = CGRectMake(0, 0, 320, y);
}

@end
