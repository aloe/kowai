//
//  Page.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PageDelegate <NSObject>

-(void) onShowArticle;
-(void) onHideArticle;

@end

@interface Page : NSObject{
    __weak NSObject<PageDelegate>* delegate;
    UINavigationController* navigationController;
}

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_ index:(int)index;
-(void) reload;
-(UIView*) getView;

@end
