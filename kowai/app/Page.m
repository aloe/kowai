//
//  Page.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Page.h"
#import "ArticleListViewController.h"

@interface Page ()<
UINavigationControllerDelegate
>

@end

@implementation Page

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_ index:(int)index{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
        navigationController = [[UINavigationController alloc] initWithRootViewController:[self createRootViewController]];
        navigationController.delegate = self;
        [navigationController setNavigationBarHidden:YES];
        navigationController.view.frame = CGRectMake(320*index, 0, 320, [VknDeviceUtil windowHeight]-64);
        
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 2)];
        if(index == 0){
            line.frame = CGRectMake(-320, 0, 640, 2);
        }else if(index == 2){
            line.frame = CGRectMake(0, 0, 640, 2);
        }
        line.backgroundColor = [self lineColor];
        [navigationController.view addSubview:line];
    }
    
    return self;
}

-(void) initialize{
    
}

-(UIViewController*) createRootViewController{
    return nil;
}

-(UIColor*)lineColor{
    return [UIColor blackColor];
}

-(void) reload{
    
}

-(UIView*)getView{
    return navigationController.view;
}

#pragma mark -------- UINavigationControllerDelegate -----------

-(void) navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    int vcCount = [navigationController.viewControllers count];
    if(vcCount == 1){
        [delegate onHideArticle];
    }else{
        [delegate onShowArticle];
    }
}

-(void) navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

@end
