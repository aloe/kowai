//
//  Api.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Api : NSObject

+(NSString*)url:(NSString*)path;
+(NSString*)articleJson:(int)index;
+(NSString*)updateArticle:(int)lastArticleId;
+(NSString*)impressionUrl;
+(NSString*)articleImpressionUrl:(int)articleId;
+(NSString*)impressionListUrl;
+(NSString*)ihanUrl;

@end
