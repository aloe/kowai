//
//  ArticleViewController.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "Article.h"

@protocol ArticleViewDelegate <NSObject>

@end

@interface ArticleViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleViewDelegate>*)delegate andArticle:(Article*)article_;
-(void) reload;

@end
