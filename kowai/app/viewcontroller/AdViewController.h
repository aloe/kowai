//
//  AdViewController.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "AdArticle.h"

@protocol AdViewDelegate <NSObject>

@end

@interface AdViewController : BaseViewController

-(id) initWithDelegate:(NSObject<AdViewDelegate>*)delegate_;
-(void) reload:(AdArticle*)adArticle_;

@end
