//
//  ArticleListViewController.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "ArticleList.h"
#import "AdArticle.h"

@protocol ArticleListViewDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;
-(void) onSelectAdArticle:(AdArticle*)adArticle;

@end

@interface ArticleListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_;
-(void) reload:(ArticleList*)articleList_;


@end
