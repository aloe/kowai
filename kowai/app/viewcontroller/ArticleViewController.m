//
//  ArticleViewController.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleViewController.h"
#import "Model.h"
#import "AdstirMraidView.h"
#import "RatingView.h"
#import "VknPostCommand.h"
#import "Api.h"
#import "VknUrlLoadCommand.h"
#import "CommentView.h"

@interface ArticleViewController ()<
UIScrollViewDelegate
, UITextFieldDelegate
, UITextViewDelegate
>{
    __weak NSObject<ArticleViewDelegate>* delegate;
    Article* article;
    UIScrollView* sv;
    UIView* headerView;
    UILabel* titleLabel;
    UIButton* favoritButton;
    float currntY;
    UIView* formView;
    RatingView* ratingView;
    UITextField* nicknameTf;
    UITextView* impressionTf;
    UIView* overlay;
    UILabel* sendButton;
    float formY;
    CommentView* commentView;
}

@end

@implementation ArticleViewController

-(id) initWithDelegate:(NSObject<ArticleViewDelegate>*)delegate_ andArticle:(Article *)article_{
    self = [super init];
    if(self){
        delegate = delegate_;
        article = article_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    view.backgroundColor = UIColorFromHex(0xaaaaaa);
    
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64)];
    sv.delegate = self;
    [view addSubview:sv];
    
    [self setCommentView];
    
    self.title = article.title;
    
    // event
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [view addGestureRecognizer:reco];
    
    // header
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    headerView.backgroundColor = [UIColor blackColor];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 0, 320-85, 44)];
    titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    titleLabel.textColor = UIColorFromHex(0xcccccc);
    [headerView addSubview:titleLabel];
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 2, 65, 40);
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:backButton];
    
    favoritButton = [UIButton buttonWithType:UIButtonTypeCustom];
    favoritButton.frame = CGRectMake(320-40-2, 2, 40, 40);
    [favoritButton setImage:[UIImage imageNamed:@"favoritOn"] forState:UIControlStateSelected];
    [favoritButton setImage:[UIImage imageNamed:@"favoritOff"] forState:UIControlStateNormal];
    [favoritButton addTarget:self action:@selector(tapFavorit) forControlEvents:UIControlEventTouchUpInside];
    favoritButton.selected = article.isFavorit;
    [headerView addSubview:favoritButton];
    
    [view addSubview:headerView];
    
    // loading
    [self showLoading];
}

-(void) setCommentView{
    // コメント
    formView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 340)];
    formView.backgroundColor = UIColorFromHex(0x660000);
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [formView addSubview:line];
    
    UILabel* commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 20)];
    commentLabel.textColor = [UIColor whiteColor];
    commentLabel.font = [UIFont systemFontOfSize:18.0f];
    commentLabel.text = @"コメントを残す";
    [formView addSubview:commentLabel];
    
    float y = 40;
    
    // 評価
    UILabel* label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 300, 20)];
    label1.textColor = [UIColor whiteColor];
    label1.font = [UIFont systemFontOfSize:14.0f];
    label1.text = @"評価:";
    [formView addSubview:label1];
     ratingView = [[RatingView alloc] init];
    CGRect frame = ratingView.frame;
    frame.origin.y = y;
    frame.origin.x = 50;
    ratingView.frame = frame;
    [formView addSubview:ratingView];
    y += 25;
    UIView* line1 = [[UIView alloc] initWithFrame:CGRectMake(10, y, 300, 1)];
    line1.backgroundColor = [UIColor whiteColor];
    [formView addSubview:line1];
    y += 15;
    
    // ニックネーム
    UILabel* label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 300, 20)];
    label2.textColor = [UIColor whiteColor];
    label2.font = [UIFont systemFontOfSize:14.0f];
    label2.text = @"ニックネーム:";
    [formView addSubview:label2];
    nicknameTf = [[UITextField alloc] initWithFrame:CGRectMake(100, y, 200, 20)];
    nicknameTf.delegate = self;
    nicknameTf.borderStyle = UITextBorderStyleRoundedRect;
    nicknameTf.text = [KowaiUtil nickname];
    nicknameTf.font = [UIFont systemFontOfSize:14.0f];
    [formView addSubview:nicknameTf];
    y += 25;
    UIView* line2 = [[UIView alloc] initWithFrame:CGRectMake(10, y, 300, 1)];
    line2.backgroundColor = [UIColor whiteColor];
    [formView addSubview:line2];
    
    // 感想
    y += 15;
    UILabel* label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 300, 20)];
    label3.textColor = [UIColor whiteColor];
    label3.font = [UIFont systemFontOfSize:14.0f];
    label3.text = @"感想:";
    [formView addSubview:label3];
    impressionTf = [[UITextView alloc] initWithFrame:CGRectMake(10, y+25, 300, 120)];
    impressionTf.backgroundColor = [UIColor whiteColor];
    impressionTf.delegate = self;
    [formView addSubview:impressionTf];
    y += 150;
    UIView* line3 = [[UIView alloc] initWithFrame:CGRectMake(10, y, 300, 1)];
    line3.backgroundColor = [UIColor whiteColor];
    [formView addSubview:line3];
    
    y+= 15;
    sendButton = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 120, 40)];
    sendButton.backgroundColor = [UIColor blackColor];
    sendButton.textColor = UIColorFromHex(0xffffff);
    sendButton.textAlignment = NSTextAlignmentCenter;
    sendButton.text = @"送信";
    sendButton.userInteractionEnabled = NO;
    [sendButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSend)]];
    [formView addSubview:sendButton];
}

-(void) tapSend{
    NSLog(@"tapSend");
    
    sendButton.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
    sendButton.alpha = 0.5f;
    [UIView animateWithDuration:0.2f animations:^{
        sendButton.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        sendButton.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
    
    [self showLoading];
    sendButton.userInteractionEnabled = NO;
    NSDictionary* values = @{
                             @"article_id": [NSNumber numberWithInt:article.id_]
                             , @"nickname": nicknameTf.text
                             , @"impression": impressionTf.text
                             , @"rate": [NSNumber numberWithInt:[ratingView rating]]
                             };
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:values withUrl:[Api impressionUrl]];
    c.onComplete = ^(NSData* data){
        [VknThreadUtil mainBlock:^{
            [KowaiUtil setNickname:nicknameTf.text];
            [self hideLoading];
            [self showToast:@"投稿しました\nご協力ありがとうございます"];
            impressionTf.text = @"";
            [self reload];
            [KowaiUtil dispatchGlobalEvent:CHANGE_FAVORIT];
        }];
    };
    c.onFail = ^(NSError* error){
        NSLog(@"post fail");
        [VknThreadUtil mainBlock:^{
            [self hideLoading];
            sendButton.userInteractionEnabled = YES;
            [self showToast:@"投稿に失敗しました\n時間を置いて再度お試しください"];
        }];
    };
    [c execute:nil];
}

-(void) tapBack{
     [self.navigationController popViewControllerAnimated:YES];
}

-(void) swipeRight{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) tapFavorit{
    article.isFavorit = !article.isFavorit;
    favoritButton.selected = article.isFavorit;
    [[Model getInstance] changeFavorit:article];
}

-(void) showHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, -44);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -------- UISCrollViewDelegate -----------

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float move = y - currntY;
    float sa = scrollView.contentSize.height - scrollView.frame.size.height - y;
    if(sa < 0) return;
    
    if(move > 0 && y > 0){
        [self hideHeader];
    }
    
    if(move < 0){
        [self showHeader];
    }
    
    currntY = y;
}

-(void)hideOverlay{
    [UIView animateWithDuration:0.2f animations:^{
        overlay.alpha = 0;
    } completion:^(BOOL finished) {
        [overlay removeFromSuperview];
        overlay = nil;
        [nicknameTf resignFirstResponder];
        [impressionTf resignFirstResponder];
        nicknameTf.transform = CGAffineTransformMakeTranslation(0, 0);
        impressionTf.transform = CGAffineTransformMakeTranslation(0, 0);
        
        [formView addSubview:nicknameTf];
        [formView addSubview:impressionTf];
        
        sendButton.userInteractionEnabled = ([nicknameTf.text length] > 0 && [impressionTf.text length] > 3);
    }];
}

-(void) showOverlay{
    float toY = sv.contentSize.height - sv.frame.size.height - 62;
    [sv setContentOffset:CGPointMake(0, formY) animated:YES];
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    overlay.backgroundColor = [UIColor blackColor];
    overlay.alpha = 0;
    [overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideOverlay)]];
    [[KowaiUtil rootViewController].view addSubview:overlay];
    [UIView animateWithDuration:0.2f animations:^{
        overlay.alpha = 0.8f;
    }];
}

#pragma mark -------- UITextFieldDelegate -----------

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self showOverlay];
    
    [VknThreadUtil threadBlock:^{
        [NSThread sleepForTimeInterval:0.2f];
        [VknThreadUtil mainBlock:^{
            [[KowaiUtil rootViewController].view addSubview:textField];
        }];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self hideOverlay];
    return YES;
}

#pragma mark -------- UITextViewDelegate -----------

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self showOverlay];
    
    [VknThreadUtil threadBlock:^{
        [NSThread sleepForTimeInterval:0.2f];
        [VknThreadUtil mainBlock:^{
            [[KowaiUtil rootViewController].view addSubview:impressionTf];
        }];
    }];
}

#pragma mark -------- public -----------

-(void) reload{
    NSArray* subViews = sv.subviews;
    for(UIView* view in subViews){
        [view removeFromSuperview];
    }
    titleLabel.text = article.title;
    [[Model getInstance] createDetailArticle:article.id_ callback:^(Article *article_) {
        float y = 40+10;
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 300, 1000000)];
        label.font = [UIFont systemFontOfSize:[KowaiUtil fontSize]];
        label.numberOfLines = 0;
        label.text = article_.content;
        [label sizeToFit];
        [sv addSubview:label];
        
        y += label.frame.size.height + 20;
        
        formY = y + 64;
        formView.transform = CGAffineTransformMakeTranslation(0, y);
        y += formView.frame.size.height + 20;
        
        AdstirMraidView* adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize300x250 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_PAGE];
        adView.transform = CGAffineTransformMakeTranslation(10, y);
        [adView start];
        [sv addSubview:adView];
        
        y += adView.frame.size.height + 0;
        
        sv.contentSize = CGSizeMake(320, y);
        
        [self hideLoading];
        
        [[Model getInstance] readArticle:article_.id_];
        [self loadImpressionList];
        
        [sv addSubview:formView];
    }];
}

-(void) loadImpressionList{
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api articleImpressionUrl:article.id_]];
    c.onComplete = ^(NSData* data){
        NSArray* array = [VknDataUtil dataToJsonArray:data];
        if(array == nil) return;
        
        CommentList* commentList = [[CommentList alloc] initWithArray:array];
        commentView = [[CommentView alloc] init];
        [commentView reload:commentList];
        
        CGSize contentSize = sv.contentSize;
        commentView.transform = CGAffineTransformMakeTranslation(0, contentSize.height);
        contentSize.height += commentView.frame.size.height + 20;
        [VknThreadUtil mainBlock:^{
            [sv addSubview:commentView];
            sv.contentSize = contentSize;
        }];
    };
    c.onFail = ^(NSError* error){
        
    };
    
    [c execute:nil];
}

@end
