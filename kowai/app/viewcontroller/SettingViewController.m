//
//  SettingViewController.m
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()<
UITableViewDataSource
, UITableViewDelegate
, UIPickerViewDataSource
, UIPickerViewDelegate
>{
    __weak NSObject<SettingViewDelegate>* delegate;
    UITableView* tableView_;
    UIPickerView* pickerView;
    UIView* overlay;
}

@end

@implementation SettingViewController

-(id) initWithDelegate:(NSObject<SettingViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64) style:UITableViewStyleGrouped];
    tableView_.delegate = self;
    tableView_.dataSource = self;
//    tableView_.backgroundColor = UIColorFromHex(0x444444);
    [self.view addSubview:tableView_];
}

-(void) showPickerView{
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    overlay.backgroundColor = [UIColor blackColor];
    [overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePicker)]];
    overlay.alpha = 0;
    [self.view addSubview:overlay];
    
    pickerView = [[UIPickerView alloc] init];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    CGRect pickerFrame = pickerView.frame;
    pickerFrame.origin.y = self.view.frame.size.height;
    pickerView.frame = pickerFrame;
    [pickerView selectRow:[KowaiUtil fontSizeIndex] inComponent:0 animated:NO];
    [self.view addSubview:pickerView];
    
    [UIView animateWithDuration:0.2f animations:^{
        overlay.alpha = 0.8f;
        pickerView.transform = CGAffineTransformMakeTranslation(0, -pickerFrame.size.height);
    } completion:^(BOOL finished) {
    }];
}

-(void) hidePicker{
    [UIView animateWithDuration:0.2f animations:^{
        overlay.alpha = 0.0f;
        pickerView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        [overlay removeFromSuperview];
        [pickerView removeFromSuperview];
        overlay = nil;
        pickerView = nil;
    }];
}

#pragma mark -------- UIPickerViewDelegate -----------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return (row == 0) ? @"小" : @"大";
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [KowaiUtil setFontSizeIndex:row];
    [tableView_ reloadData];
}

#pragma mark -------- UITableViewDelegate -----------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch(section) {
        case 0:
            return @"設定";
            break;
        case 1:
            return @"アプリケーション情報";
            break;
    }
    return nil; //ビルド警告回避用
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }else if(section == 1){
        return 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if(indexPath.section == 0){
        cell.textLabel.text = @"文字サイズ";
        cell.detailTextLabel.text = ([KowaiUtil fontSizeIndex] == 0) ? @"小" : @"大";
    }else{
        if(indexPath.row == 0){
            cell.textLabel.text = @"レビューを書く(おねがいします！)";
        }else{
            cell.textLabel.text = @"アプリバージョン";
            cell.detailTextLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView_ deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0){
        [self showPickerView];
    }else{
        if(indexPath.row == 0){
            [VknUtils openBrowser:APPSTORE_URL];
        }
    }
}


@end
