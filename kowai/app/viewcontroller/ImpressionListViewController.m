//
//  ImpressionListViewController.m
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ImpressionListViewController.h"
#import "ArticleCell.h"
#import "CommentList.h"
#import "AdArticleCell.h"
#import "AdstirMraidView.h"
#import "Model.h"
#import "Ad.h"

#define AD_PER_PAGE 10

@interface ImpressionListViewController ()<
UITableViewDataSource
, UITableViewDelegate
, ArticleCellDelegate
, AdArticleCellDelegate
>{
    __weak NSObject<ImpressionListViewDelegate>* delegate;
    UITableView* tableView_;
    CommentList* commentList;
}

@end

@implementation ImpressionListViewController

-(id) initWithDelegate:(NSObject<ImpressionListViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 2, 320, [VknDeviceUtil windowHeight]-64-2)];
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = [UIColor blackColor];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    UIView* adWrap = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 110)];
    AdstirMraidView* adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_LIST];
    adView.transform = CGAffineTransformMakeTranslation(0, 5);
    [adView start];
    [adWrap addSubview:adView];
    //    adView.backgroundColor = [UIColor whiteColor];
    tableView_.tableFooterView = adWrap;
    
    UIView* view = self.view;
    [view addSubview:tableView_];
    
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x50 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_ALWAYS];
    adView.frame = CGRectMake(0, [VknDeviceUtil windowHeight]-64-50, 320, 50);
    //    adView.backgroundColor = [UIColor whiteColor];
    [adView start];
    [self.view addSubview:adView];
}

-(BOOL) isAdIndex:(NSIndexPath*)indexPath{
    int row = indexPath.row;
    return (row != 0 && row % AD_PER_PAGE == 0 );
}


-(int) modelIndex:(NSIndexPath*)indexPath{
    int row = indexPath.row;
    int modelIndex = row - (row / AD_PER_PAGE);
    
    return modelIndex;
}

-(Comment*)model:(NSIndexPath*)indexPath{
    return [commentList get:[self modelIndex:indexPath]];
}

#pragma mark -------- public -----------

-(void) reload:(CommentList*)commentList_{
    commentList = commentList_;
    [tableView_ reloadData];
}

#pragma mark -------- AdArticleCellDelegate -----------

-(void) onSelectAdArticle:(AdArticle *)adArticle{
    [delegate onSelectAdArticle:adArticle];
}

#pragma mark -------- UITableViewDelegate -----------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([commentList count] == 0) return 0;
    int p = [commentList count] / AD_PER_PAGE;
    
    int p2 = p/AD_PER_PAGE;
    if(p2 % AD_PER_PAGE != 0) p2++;
    
    return [commentList count] + p + p2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    NSString *adCellIdentifier = @"AdCell";
    
    if([self isAdIndex:indexPath]){
        AdArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:adCellIdentifier];
        
        if (!cell) {
            cell = [[AdArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:adCellIdentifier];
            [cell setDelegate:self];
        }
        
        [cell reload:[Ad adArticle] index:indexPath.row];
        return cell;
    }
    
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[ArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setDelegate:self];
    }
    
    Comment* comment = [self model:indexPath];
    [cell reloadComment:comment index:indexPath.row];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self isAdIndex:indexPath]) return;
    
    [tableView_ deselectRowAtIndexPath:indexPath animated:YES];
    
    ArticleCell* articleCell = [tableView_ cellForRowAtIndexPath:indexPath];
    [articleCell light];
    
    Comment* comment = [self model:indexPath];
    [delegate onSelectComment:comment];
}

@end
