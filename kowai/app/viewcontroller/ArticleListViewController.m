//
//  ArticleListViewController.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleListViewController.h"
#import "ArticleCell.h"
#import "ArticleList.h"
#import "AdArticleCell.h"
#import "AdstirMraidView.h"
#import "Model.h"
#import "Ad.h"

#define AD_PER_PAGE 10

@interface ArticleListViewController ()<
UITableViewDataSource
, UITableViewDelegate
, ArticleCellDelegate
, AdArticleCellDelegate
>{
    __weak NSObject<ArticleListViewDelegate>* delegate;
    UITableView* tableView_;
    ArticleList* articleList;
}

@end

@implementation ArticleListViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 2, 320, [VknDeviceUtil windowHeight]-64-2)];
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = [UIColor blackColor];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    UIView* adWrap = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 110)];
    AdstirMraidView* adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_LIST];
    adView.transform = CGAffineTransformMakeTranslation(0, 5);
    [adView start];
    [adWrap addSubview:adView];
//    adView.backgroundColor = [UIColor whiteColor];
    tableView_.tableFooterView = adWrap;
    
    UIView* view = self.view;
    [view addSubview:tableView_];
    
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x50 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_ALWAYS];
    adView.frame = CGRectMake(0, [VknDeviceUtil windowHeight]-64-50, 320, 50);
//    adView.backgroundColor = [UIColor whiteColor];
    [adView start];
    [self.view addSubview:adView];
}

-(BOOL) isAdIndex:(NSIndexPath*)indexPath{
    if(![KowaiUtil isJaDevice]){
        return NO;
    }
    int row = indexPath.row;
    return (row != 0 && row % AD_PER_PAGE == 0 );
}


-(int) modelIndex:(NSIndexPath*)indexPath{
    if(![KowaiUtil isJaDevice]){
        return indexPath.row;
    }
    
    int row = indexPath.row;
    int modelIndex = row - (row / AD_PER_PAGE);
    
    return modelIndex;
}

-(Article*)model:(NSIndexPath*)indexPath{
    return [articleList get:[self modelIndex:indexPath]];
}

#pragma mark -------- public -----------

-(void) reload:(ArticleList*)articleList_{
    articleList = articleList_;
    [tableView_ reloadData];
}

#pragma mark -------- AdArticleCellDelegate -----------

-(void) onSelectAdArticle:(AdArticle *)adArticle{
    [delegate onSelectAdArticle:adArticle];
}

#pragma mark -------- UITableViewDelegate -----------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(![KowaiUtil isJaDevice]){
        return [articleList count];
    }
    
    if([articleList count] == 0) return 0;
    int p = [articleList count] / AD_PER_PAGE;
    
    int p2 = p/AD_PER_PAGE;
//    if(p2 % AD_PER_PAGE != 0) p2++;
    
    return [articleList count] + p + p2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    NSString *adCellIdentifier = @"AdCell";
    
    if([self isAdIndex:indexPath]){
        AdArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:adCellIdentifier];
        
        if (!cell) {
            cell = [[AdArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:adCellIdentifier];
            [cell setDelegate:self];
        }
        
        [cell reload:[Ad adArticle] index:indexPath.row];
        return cell;
    }
    
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[ArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setDelegate:self];
    }
    
    Article* article = [self model:indexPath];
    [cell reload:article index:indexPath.row];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self isAdIndex:indexPath]) return;
    
    [tableView_ deselectRowAtIndexPath:indexPath animated:YES];
    
    ArticleCell* articleCell = [tableView_ cellForRowAtIndexPath:indexPath];
    [articleCell light];
    
    Article* article = [self model:indexPath];
    [delegate onSelectArticle:article];
}

@end
