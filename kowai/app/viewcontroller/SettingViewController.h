//
//  SettingViewController.h
//  kowai
//
//  Created by kawase yu on 2014/08/15.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"

@protocol SettingViewDelegate <NSObject>

@end

@interface SettingViewController : BaseViewController

-(id) initWithDelegate:(NSObject<SettingViewDelegate>*)delegate_;

@end
