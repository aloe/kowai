//
//  AdViewController.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdViewController.h"
#import "VknURLLoadCommand.h"

@interface AdViewController ()<
UIScrollViewDelegate
>{
    __weak NSObject<AdViewDelegate>* delegate;
    AdArticle* adArticle;
    UIView* headerView;
    UILabel* titleLabel;
    
    UILabel* detailLabel;
    UILabel* descriptionLabel;
    UILabel* ratingLabel;
    UILabel* linkButton;
    UIImageView* imageView;
    UIScrollView* sv;
    UIView* header;
    UILabel* headerTitleLabel;
    
    UIScrollView* screenshotSv;
    float currntY;
}

@end

@implementation AdViewController

-(id) initWithDelegate:(NSObject<AdViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = UIColorFromHex(0xaaaaaa);
    [self setContents];
    [self setHeader];
    
    // event
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tapBack)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];

}

-(void) setHeader{
    // header
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    headerView.backgroundColor = [UIColor blackColor];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 0, 320-75, 44)];
    titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    titleLabel.textColor = UIColorFromHex(0xcccccc);
    [headerView addSubview:titleLabel];
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 2, 65, 40);
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:backButton];
    
    [self.view addSubview:headerView];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) setContents{
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64)];
    sv.delegate = self;
    [self.view addSubview:sv];
    
    float y = 60;
    imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(110, y, 100, 100);
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 20;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    
    [sv addSubview:imageView];
    
    ratingLabel = [[UILabel alloc] init];
    ratingLabel.font = [UIFont systemFontOfSize:13.0f];
    ratingLabel.numberOfLines = 2;
    [sv addSubview:ratingLabel];
    
    detailLabel = [[UILabel alloc] init];
    detailLabel.numberOfLines = 0;
    [sv addSubview:detailLabel];
    
    descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.numberOfLines = 0;
    [sv addSubview:descriptionLabel];
    
    linkButton = [[UILabel alloc] init];
    linkButton.font = [UIFont boldSystemFontOfSize:18.0f];
    linkButton.textAlignment = NSTextAlignmentCenter;
    linkButton.textColor = [UIColor whiteColor];
    linkButton.text = @"ダウンロード";
    linkButton.userInteractionEnabled = YES;
    [linkButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapButton)]];
    [sv addSubview:linkButton];
    
    screenshotSv = [[UIScrollView alloc] init];
    [sv addSubview:screenshotSv];
}

-(void) tapButton{
    [VknUtils openBrowser:adArticle.linkUrl];
}

-(void) reload:(AdArticle*)adArticle_{
    adArticle = adArticle_;
    float y = 20 + 101 + 20 + 40;
    
    titleLabel.text = adArticle.title;
    
    headerTitleLabel.text = adArticle.title;
    
    ratingLabel.frame = CGRectMake(10, y, 300, 40);
    y += 40+15;
    
    screenshotSv.frame = CGRectMake(0, y, 320, 372);
    screenshotSv.backgroundColor = UIColorFromHex(0xeeeeee);
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(0, 371.5f, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    
    y += 372 + 15;
    
    descriptionLabel.frame = CGRectMake(10, y, 300, 10000);
    descriptionLabel.text = adArticle.content;
    [descriptionLabel sizeToFit];
    y += descriptionLabel.frame.size.height + 15;
    
    detailLabel.frame = CGRectMake(10, y, 300, 10000);
    detailLabel.text = adArticle.detailContent;
    [detailLabel sizeToFit];
    y += detailLabel.frame.size.height + 15;
    
    linkButton.frame = CGRectMake(50, y, 220, 44);
    linkButton.backgroundColor = UIColorFromHex(0x444444);
    y += linkButton.frame.size.height + 15 + 10 + 45;
    
    sv.contentSize = CGSizeMake(320, y);
    
    [VknImageCache loadImage:adArticle.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:adArticle.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
    
    [self loadApi];
}

-(void) loadApi{
    NSString* api = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?country=JP&id=%@", adArticle.appId];
    NSLog(@"%@", api);
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:api];
    c.onComplete = ^(NSData* data){
        NSDictionary* jsonDic = [VknDataUtil dataToJson:data];
        if(jsonDic == nil) return;
        NSLog(@"%@", jsonDic);
        NSArray* results = [jsonDic objectForKey:@"results"];
        if(results == nil) return;
        if([results count] == 0) return;
        NSDictionary* result = [results firstObject];
        NSArray* urls = [result objectForKey:@"screenshotUrls"];
        float rating = [[result objectForKey:@"averageUserRatingForCurrentVersion"] floatValue];
        int ratingCount = [[result objectForKey:@"userRatingCountForCurrentVersion"] intValue];
        NSString* version = [result objectForKey:@"version"];
        
        ratingLabel.text = [NSString stringWithFormat:@"【評価：%d】 \n%d件の評価(バージョン%@)", (int)rating, ratingCount, version];
        for(int i=0,max=[urls count];i<max;i++){
            NSString* url = [urls objectAtIndex:i];
            [self loadScreenshot:url index:i];
        }
        
        screenshotSv.contentSize = CGSizeMake(([urls count] * 210) + 10, 372);
    };
    
    [c execute:nil];
}

-(void) loadScreenshot:(NSString*)url index:(int)index{
    CGRect frame = CGRectMake(index * 210+10, 10, 200, 352);
    UIImageView* ssView = [[UIImageView alloc] initWithFrame:frame];
    [screenshotSv addSubview:ssView];
    [VknImageCache loadImage:url defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        [VknThreadUtil mainBlock:^{
            ssView.image = image;
        }];
    }];
}

-(void) showHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, -44);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -------- UISCrollViewDelegate -----------

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float move = y - currntY;
    float sa = scrollView.contentSize.height - scrollView.frame.size.height - y;
    if(sa < 0) return;
    
    if(move > 0 && y > 0){
        [self hideHeader];
    }
    
    if(move < 0){
        [self showHeader];
    }
    
    currntY = y;
}


@end
