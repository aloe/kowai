//
//  RootViewController.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController (){
    __weak NSObject<RootViewDelegate>* delegate;
}

@end

@implementation RootViewController

-(id) initWithDelegate:(NSObject<RootViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    
}

@end
