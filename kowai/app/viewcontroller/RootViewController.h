//
//  RootViewController.h
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"

@protocol RootViewDelegate <NSObject>

@end

@interface RootViewController : BaseViewController

-(id) initWithDelegate:(NSObject<RootViewDelegate>*)delegate_;

@end
