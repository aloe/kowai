//
//  ImpressionListViewController.h
//  kowai
//
//  Created by kawase yu on 2014/08/23.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "CommentList.h"
#import "AdArticle.h"

@protocol ImpressionListViewDelegate <NSObject>

-(void) onSelectComment:(Comment*)comment;
-(void) onSelectAdArticle:(AdArticle*)adArticle;

@end

@interface ImpressionListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ImpressionListViewDelegate>*)delegate_;
-(void) reload:(CommentList*)commentList_;

@end
