//
//  KowaiUtil.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "KowaiUtil.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#include <sys/xattr.h>

@interface KowaiUtil ()

+(void) noReview;

@end

@interface AlertDelegate : NSObject<UIAlertViewDelegate>

@end

@implementation AlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // レビューする
        [VknUtils openBrowser:APPSTORE_URL];
        [KowaiUtil noReview];
    }else if(buttonIndex == 1){
        // あとで
        return;
    }else if(buttonIndex == 2){
        // レビューしない
        [KowaiUtil noReview];
    }
}

@end

@implementation KowaiUtil

static UIViewController* rootViewController;
static BOOL isJaDevice;
+(void) setup:(UIViewController*)rootViewController_{
    rootViewController = rootViewController_;
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString* countryCode = carrier.mobileCountryCode;
    isJaDevice = ([countryCode isEqualToString:@"440"] || [countryCode isEqualToString:@"441"]);
}

+(UIViewController*)rootViewController{
    return rootViewController;
}

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name{
    NSNotificationCenter* c = [NSNotificationCenter defaultCenter];
    [c addObserver:target selector:selector name:name object:nil];
}

+(void) dispatchGlobalEvent:(NSString*)name{
    NSNotification *n = [NSNotification notificationWithName:name object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}


static NSString* fontSizeKey = @"fontSizeKey";
+(int) fontSizeIndex{
    return [[NSUserDefaults standardUserDefaults] integerForKey:fontSizeKey];
}

+(void) setFontSizeIndex:(int)index{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setInteger:index forKey:fontSizeKey];
    [ud synchronize];
}

+(float) fontSize{
    return ([self fontSizeIndex] == 0) ? 16.0f : 19.0f;
}

static NSString* showedCountKey = @"showedCountKey";
static AlertDelegate* alertDelegate ;
+(void) tryShowRecommend{
    if([self isNoReview]) return;
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    int current = [ud integerForKey:showedCountKey];
    current++;
    [ud setInteger:current forKey:showedCountKey];
    [ud synchronize];
    
    if(!(current == 5 || current == 15 || current == 40 || current == 100)) return;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"ご意見お待ちしております";
    alertView.message = @"いつもご利用ありがとうございます。\nまだまだ貧弱なアプリですが、随時アップデートしてい予定です。\nこんな機能欲しい！\nここが使いにくい！\n等ありましたら、ぜひレビューをお願いしますm(^_^)m。";
    [alertView addButtonWithTitle:@"レビューする"];
    [alertView addButtonWithTitle:@"あとで"];
    [alertView addButtonWithTitle:@"レビューしない"];
    
    alertDelegate = [[AlertDelegate alloc] init];
    alertView.delegate = alertDelegate;
    
    [alertView show];
}

static NSString* noReviewKey = @"noReviewKey";
+(BOOL) isNoReview{
    return [[NSUserDefaults standardUserDefaults] boolForKey:noReviewKey];
}

+(void) noReview{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:noReviewKey];
    [ud synchronize];
}

static NSString* nicknameKey = @"nicknameKey";
+(void) setNickname:(NSString*)nickname{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:nickname forKey:nicknameKey];
    [ud synchronize];
}

+(NSString*)nickname{
    return [[NSUserDefaults standardUserDefaults] stringForKey:nicknameKey];
}

+(BOOL) isJaDevice{
    return isJaDevice;
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}


@end
