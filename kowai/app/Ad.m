//
//  Ad.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Ad.h"
#import "ChkControllerDelegate.h"
#import "ChkController.h"

static ChkController* chkController;
static AdArticleList* adcropsArticleList;

@interface ChkDelegate:NSObject<ChkControllerDelegate>

@end

@implementation ChkDelegate

- (void) chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess");
    if([chkController hasNextData]){
//        [VknThreadUtil threadBlock:^{
            [chkController requestDataList];
//        }];
    }
    
    NSArray* dataList = chkController.dataList;
    
    if([dataList count] == 0) return;
    adcropsArticleList = [[AdArticleList alloc] initWithChkDatalist:dataList];
}

- (void) chkControllerDataListWithError:(NSError*)error{
    
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{
    
}

@end

@implementation Ad

static ChkDelegate* chkDelegate;

+(void) setup{
    adcropsArticleList = [AdArticleList createDummy];
}

+(void) reload{
    adcropsArticleList = [AdArticleList createDummy];
    
    chkDelegate = [[ChkDelegate alloc] init];
    chkController = [[ChkController alloc] initWithDelegate:chkDelegate];
    
//    [VknThreadUtil threadBlock:^{
        [chkController requestDataList];
//    }];
}

+(AdArticle*)adArticle{
    if([adcropsArticleList count] == 0) return nil;
    
    int r = arc4random_uniform([adcropsArticleList count]);
    return [adcropsArticleList get:r];
}

@end
