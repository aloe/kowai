//
//  AppDelegate.m
//  kowai
//
//  Created by kawase yu on 2014/08/14.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AppDelegate.h"
#import "Mediator.h"
#import "ChkApplicationOptional.h"

@interface AppDelegate (){
    Mediator* mediator;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    mediator = [[Mediator alloc] initWithWindow:self.window];
    
    //    https://gist.github.com/higepon/984a4cd5715f89980e1a
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        [application registerForRemoteNotificationTypes: UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge];
    }
#else
    [application registerForRemoteNotificationTypes: UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge];
#endif
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [mediator applicationDidEnterBackground:application];
    [ChkApplicationOptional applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [mediator applicationWillEnterForeground:application];
    [ChkApplicationOptional applicationWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

-(void) applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [mediator memoryWarning];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) token {
    NSLog(@"getDevicetoken");
    [mediator application:application didRegisterForRemoteNotificationsWithDeviceToken:token];
}
//
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
//{
//    //register to receive notifications
//    [application registerForRemoteNotifications];
//}

@end
